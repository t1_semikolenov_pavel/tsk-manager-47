package ru.t1.semikolenov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.model.IProjectRepository;
import ru.t1.semikolenov.tm.api.repository.model.ITaskRepository;
import ru.t1.semikolenov.tm.api.service.IConnectionService;
import ru.t1.semikolenov.tm.api.service.model.IProjectTaskService;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.model.Task;
import ru.t1.semikolenov.tm.repository.model.ProjectRepository;
import ru.t1.semikolenov.tm.repository.model.TaskRepository;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
        this.taskRepository = new TaskRepository(connectionService.getEntityManager());
        this.projectRepository = new ProjectRepository(connectionService.getEntityManager());
    }

    @Override
    public void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Project project = projectRepository.findOneById(projectId);
        task.setProject(project);
        taskRepository.update(task);
    }

    @Override
    public void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(null);
        taskRepository.update(task);
    }

    @Override
    public void removeProjectById(@NotNull String userId, @NotNull String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.removeTasksByProjectId(userId, projectId);
    }

}
