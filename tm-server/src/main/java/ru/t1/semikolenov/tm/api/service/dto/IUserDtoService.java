package ru.t1.semikolenov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.dto.model.UserDTO;
import ru.t1.semikolenov.tm.enumerated.Role;

public interface IUserDtoService extends IDtoService<UserDTO> {

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password);

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @Nullable Role role);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @NotNull
    void removeByLogin(@NotNull String login);

    @NotNull
    UserDTO setPassword(@NotNull String id, @NotNull String password);

    @NotNull
    UserDTO updateUser(
            @NotNull String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}
