package ru.t1.semikolenov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.dto.IDtoRepository;
import ru.t1.semikolenov.tm.api.service.IConnectionService;
import ru.t1.semikolenov.tm.api.service.dto.IDtoService;
import ru.t1.semikolenov.tm.dto.model.AbstractModelDTO;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract IDtoRepository<M> getRepository(@NotNull final EntityManager entityManager);

    public void add(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.set(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void remove(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getCount() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.getCount();
        } finally {
            entityManager.close();
        }
    }

}
