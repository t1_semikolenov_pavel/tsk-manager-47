package ru.t1.semikolenov.tm.service;

public class ProjectTaskServiceTest {

//    @NotNull
//    private final ITaskRepository taskRepository = new TaskRepository();
//
//    @NotNull
//    private final IProjectRepository projectRepository = new ProjectRepository();
//
//    @NotNull
//    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
//
//    @NotNull
//    private final String USER_ID = UUID.randomUUID().toString();
//
//    @Before
//    public void init() {
//        projectRepository.create(USER_ID, "project-1");
//        taskRepository.create(USER_ID, "task-1");
//        taskRepository.create(USER_ID, "task-2");
//    }
//
//    @Test
//    public void bindTaskToProject() {
//        @NotNull final Project project = projectRepository.findOneByIndex(USER_ID, 0);
//        @NotNull final String projectId = project.getId();
//        @NotNull final Task task = taskRepository.findOneByIndex(USER_ID, 0);
//        @NotNull final String taskId = task.getId();
//        Assert.assertThrows(EmptyUserIdException.class,
//                () -> projectTaskService.bindTaskToProject("", projectId, taskId));
//        Assert.assertThrows(EmptyIdException.class,
//                () -> projectTaskService.bindTaskToProject(USER_ID, "", taskId));
//        Assert.assertThrows(EmptyIdException.class,
//                () -> projectTaskService.bindTaskToProject(USER_ID, projectId, ""));
//        Assert.assertThrows(ProjectNotFoundException.class,
//                () -> projectTaskService.bindTaskToProject(USER_ID, "project_id", taskId));
//        Assert.assertThrows(TaskNotFoundException.class,
//                () -> projectTaskService.bindTaskToProject(USER_ID, projectId, "task_id"));
//        projectTaskService.bindTaskToProject(USER_ID, projectId, taskId);
//        Assert.assertNotNull(task.getProjectId());
//        Assert.assertEquals(projectId, task.getProjectId());
//    }
//
//    @Test
//    public void unbindTaskFromProject() {
//        @NotNull final Project project = projectRepository.findOneByIndex(USER_ID, 0);
//        @NotNull final String projectId = project.getId();
//        @NotNull final Task task = taskRepository.findOneByIndex(USER_ID, 0);
//        @NotNull final String taskId = task.getId();
//        Assert.assertThrows(EmptyUserIdException.class,
//                () -> projectTaskService.unbindTaskFromProject("", projectId, taskId));
//        Assert.assertThrows(EmptyIdException.class,
//                () -> projectTaskService.unbindTaskFromProject(USER_ID, "", taskId));
//        Assert.assertThrows(EmptyIdException.class,
//                () -> projectTaskService.unbindTaskFromProject(USER_ID, projectId, ""));
//        Assert.assertThrows(ProjectNotFoundException.class,
//                () -> projectTaskService.unbindTaskFromProject(USER_ID, "project_id", taskId));
//        Assert.assertThrows(TaskNotFoundException.class,
//                () -> projectTaskService.unbindTaskFromProject(USER_ID, projectId, "task_id"));
//        task.setProjectId(projectId);
//        Assert.assertNotNull(task.getProjectId());
//        projectTaskService.unbindTaskFromProject(USER_ID, projectId, taskId);
//        Assert.assertNull(task.getProjectId());
//    }

}
