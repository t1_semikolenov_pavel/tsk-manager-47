package ru.t1.semikolenov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataJsonSaveJaxbRequest extends AbstractUserRequest {

    public DataJsonSaveJaxbRequest(@Nullable String token) {
        super(token);
    }

}
