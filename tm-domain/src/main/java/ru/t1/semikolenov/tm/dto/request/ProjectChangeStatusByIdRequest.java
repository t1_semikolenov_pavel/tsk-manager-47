package ru.t1.semikolenov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIdRequest(
            @Nullable final String token,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        super(token);
        this.id = id;
        this.status = status;
    }

}